import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Link } from "./Link";

export const FEED_QUERY = gql`
  {
    feed {
      links {
        id
        createdAt
        url
        description
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
    }
  }
`;

export function LinkList() {
  const _updateCacheAfterVote = (store, createVote, linkId) => {
    const data = store.readQuery({ query: FEED_QUERY });
    const votedLink = data.feed.links.find((link) => link.id === linkId);
    votedLink.votes = createVote.link.votes;

    store.writeQuery({ query: FEED_QUERY, data });
  };

  return (
    <Query query={FEED_QUERY}>
      {({ loading, error, data }) => {
        if (loading) return <div>Fetching</div>;
        if (error) return <div>Error</div>;

        return data.feed.links.map((link, idx) => (
          <Link
            key={link.id}
            link={link}
            index={idx}
            updateStoreAfterVote={_updateCacheAfterVote}
          />
        ));
      }}
    </Query>
  );
}

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Mutation } from "react-apollo";
import { FEED_QUERY } from "./LinkList";
import gql from "graphql-tag";

const POST_MUTATION = gql`
  mutation PostMutation($description: String!, $url: String!) {
    post(description: $description, url: $url) {
      id
      createdAt
      url
      description
    }
  }
`;

export function CreateLink() {
  const history = useHistory();
  const [description, setDescription] = useState();
  const [url, setUrl] = useState();

  const updateInput = (event) => setDescription(event.target.value);

  return (
    <div>
      <div className="flex flex-column mt3">
        <input
          className="mb2"
          value={description}
          onChange={updateInput}
          type="text"
          placeholder="A description for the link"
        />
        <input
          className="mb2"
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          type="text"
          placeholder="A URL for the link"
        />
        <Mutation
          mutation={POST_MUTATION}
          variables={{ description, url }}
          onCompleted={() => history.push("/")}
          update={(store, { data: { post } }) => {
            const data = store.readQuery({ query: FEED_QUERY });
            data.feed.links.unshift(post);
            store.writeQuery({ query: FEED_QUERY, data });
          }}
        >
          {(postMutation) => <button onClick={postMutation}>Submit</button>}
        </Mutation>
      </div>
    </div>
  );
}

import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { LinkList } from "./LinkList";
import { CreateLink } from "./CreateLink";
import "../styles/App.css";
import { Header } from "./Header";
import { Login } from "./Login";
import Search from "./Search";

function App() {
  return (
    <div className="App">
      <Router>
        <Header className="center w85" />
        <Switch>
          <Route exact path="/">
            <LinkList />
          </Route>
          <Route exact path="/create">
            <CreateLink />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/search">
            <Search />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
